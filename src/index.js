const ContentStrategy = require('./db/strateges/base/ContentStrategy')
const MongoDB = require('./db/strateges/mongodb')
const Postgres = require('./db/strateges/postgres')

const contextMongo = new ContentStrategy(new MongoDB())
contextMongo.create()

const contextPostgres = new ContentStrategy(new Postgres())
contextPostgres.create()