const Sequelize = require('sequelize')


const HeroiSchema = {
  name: 'postgres',
  schema: {  
    id: {
        type: Sequelize.INTEGER,
        required: true,
        primaryKey: true,
        autoIncrement: true
    },
    nome: {
        type: Sequelize.TEXT
        
        ,
        required:true
    },
    poder:{
        type: Sequelize.TEXT,
        required: true
    }

    },
    options: {
        tableName: 'TB_HEROIS',
        freezeTableName: false,
        timestamps: false,
        
    }
}
 

module.exports = HeroiSchema
