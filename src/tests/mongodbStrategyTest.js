const assert = require('assert')
const MongoDb = require('../../src/db/strateges/mongodb/mongodb')
const HeroisSchema = require('./../db/strateges/mongodb/schemas/heroisSchema')
const Context = require('../../src/db/strateges/base/ContentStrategy')

const MOCK_HEROI_CADASTRAR = {
    nome: 'Mulher Maravilha',
    poder: 'Laço'
}

const MOCK_HEROI_DEFAULT = {
    nome: `Ciclope-${Date.now()}` ,
    poder: 'Olho laser'
}

const MOCK_HEROI_ATUALIZAR = {
    nome: `Patolino-${Date.now()}` ,
    poder: 'Velocidade'
}

let MOCK_HEROI_ID =''

let context = {}

describe('MongoDB suite de teste', function ()  {
this.beforeAll(async () => {
    const connection = MongoDb.connect()
    context = new Context(new MongoDb(connection, HeroisSchema))
    await context.create(MOCK_HEROI_DEFAULT)
    const result = await context.create(MOCK_HEROI_ATUALIZAR)
    MOCK_HEROI_ID = result._id
})
    it('vereficar conexão', async () => {
        const result = await context.isConnected()
        const expected = 'Conectado'
        assert.deepEqual(result, expected)
    })
    it('cadastrar', async () => {
        const {nome , poder } = await context.create(MOCK_HEROI_CADASTRAR)

       assert.deepEqual({nome, poder}, MOCK_HEROI_CADASTRAR)

    })
    it('listar', async () =>{
     //  const result = await context.read({nome: MOCK_HEROI_DEFAULT.nome})
    //   console.log('result', result)
      const [{nome, poder }] = await context.read({nome: MOCK_HEROI_DEFAULT.nome})
       const result = {nome, poder}
       
      assert.deepEqual(result, MOCK_HEROI_DEFAULT)
    
    })
    it('atualizar', async () =>{
        const result = await context.update(MOCK_HEROI_ID, {
            nome: 'Pernalonga'
        })
        assert.deepEqual(result.nModified, 1)
    })
    it('remover', async ()=>{
        const result = await context.delete(MOCK_HEROI_ID)
        assert.deepEqual(result.n, 1)
    })
})