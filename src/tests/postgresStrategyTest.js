const assert = require('assert')
const Postgres = require('../../src/db/strateges/postgres/postgres')
const HeroiSchema = require('../db/strateges/postgres/schemas/heroiSchema')
const Context = require('../../src/db/strateges/base/ContentStrategy')


const MOCHA_HEROIS_CADASTRAR = { nome: 'Pantera negra ',    poder: 'Vibranio'}
const MOCHA_HEROIS_ATUALIZAR = { nome: 'Homem Aranha', poder: 'Teia'}


let context = {}
describe('Postgres Strategy', function(){
    this.timeout(Infinity)
    this.beforeAll(async function(){
      const connection = await Postgres.connect()
      const model = await Postgres.defineModel(connection, HeroiSchema)
      context = new Context(new Postgres(connection, model));
      await context.delete()
      await context.create(MOCHA_HEROIS_CADASTRAR)
      await context.create(MOCHA_HEROIS_ATUALIZAR)
       
    })
    it('PostgresSQL Connection', async function() {
        const result = await context.isConnected()
        assert.equal(result, true)
    })
    it('Cadastrar', async function(){
        const result = await context.create(MOCHA_HEROIS_CADASTRAR)
        console.log('resultado', result)
        delete result.id
        assert.deepEqual(result, MOCHA_HEROIS_CADASTRAR)

    })
    it('Listar', async function() {
        const [result] = await context.read({nome: MOCHA_HEROIS_CADASTRAR.nome})
        delete result.id
        assert.deepEqual(result, MOCHA_HEROIS_CADASTRAR)
    })
    it('Atualizar', async function(){
        const [itemAtualizar] = await context.read({nome: MOCHA_HEROIS_ATUALIZAR.nome })
        novoItem  = {
            ...MOCHA_HEROIS_ATUALIZAR,
            nome: 'Mulher Maravilha'
        }
        const [result] = await context.update(itemAtualizar.id, novoItem)
        const [itemAtualizado] = await context.read({id: itemAtualizar.id})
        assert.deepEqual(result, 1)
        assert.deepEqual(itemAtualizado.nome, novoItem.nome)
        /* 
        No javascript temos uma tecnica chamada rest/spread que e um metodo usado para margear objetos ou
        separa-lo 

        */
    })
    it('remover por id', async function() {
        const [item] = await context.read({})
        const result = await context.delete(item.id)
        assert.deepEqual(result, 1)
    })
})